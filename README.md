# Pre-selection-js-with-test

<hr>
<hr>

## **Preparation for the task solving**
<hr>
<hr>

1. Choose a development environment and install it (VSCode, Webstorm, Sublime, etc.)

2. Install NodeJS: https://nodejs.org/uk/ (It is advised to use node js of version 12 or lower. If you use any of the features that are not supported by v12, the score won't be submitted.

3. Fork this repository

4. Clone your newly forked repository (use git clone <repository-name> (git should be installed first: https://git-scm.com/downloads)

5. Go to the project folder 

6. Install dependencies (use npm install)

7. Run  npm test in the command line. If you see the following:
![Errors](img/errors.png)
<strong> it means that you failed the tests (“errno 3 “ means that you failed 3 tests)</strong>
<br> <strong>And if you see the following (empty result):</strong>
<br>![Without errors](img/withoutErrors.png)<br>
<strong> it means that you have passed the tests. </strong>

8. But if you’d like to see more detailed info on your results you need to change some scripts in package.json <br>
Our scripts now:
```js
"scripts": {
   "test:junit": "cross-env MOCHA_FILE=junit.xml mocha test --reporter mocha-junit-reporter",
   "test:xunit": "cross-env MOCHA_FILE=xunit.xml mocha test --reporter mocha-xunit-reporter",
   "test": "npm run test:junit",
   "build": "echo \"Dummy build script\""
 },
```  
You need to replace “test” script with this:
```js
"test": "mocha || true",
```

Run “npm test” again. The result should look like this:
![Pretty Output](img/prettyOutput.png)


### !! IMPORTANT !! Before you commit this task for Autocode testing you must return your scripts back to the defaults:
```js
"test": "npm run test:junit"
```
### Otherwise autocode tests will fail.
<br>
<br>
<hr>
<hr>

## **Tasks**

<hr>
<hr>

### TASK № 1
### Write a function “getChars”, that takes 1 parameter - an array of words. Your task is to concatenate the nth letter from each word to construct a new word which should be returned as a string. From the first word the first letter is taken, from the second word, the second letter is taken, from the third word the third letter is taken and so forth... If word doesn’t have needed amount of letters, this word is skipped.

### Example:
```js
getChars(['javascript','is cool', '123']) // returned value 'js3'
```
<hr>

### TASK № 2
### Write a function char_freq() that takes a string and builds a frequency listing of the characters contained in it.

### Example:
```js
char_freq("abbabcbdbabdbdbabababcbcbab");  // {a: 7, b: 14, c: 3, d: 3}
```
<hr>

### TASK № 3
### Write function range(start, end) which takes two integers and outputs a string with comma separated list of all integer numbers in range from `start` to `end` (including range boundaries). 

### Example:
```js
range(1, 10);  // returns "1, 2, 3, 4, 5, 6, 7, 8, 9, 10" 
```
<hr>

### TASK № 4
### Write a function listNames(users) that takes an array containing user objects and return a string formatted as a list of names separated by commas except for the last two names, which should be separated by 'and'.

### Example:
```js
listNames([ {name: 'Max'}, {name: 'Denis'}, {name: 'Maria'} ]) // returns 'Max, Denis and Maria' 
listNames([ {name: 'Denis'}, {name: 'Maria'} ]) // returns 'Denis and Maria' 
listNames([ {name: 'Max'} ])  // returns 'Max' 
listNames([]) // returns ''” 

```
<hr>

### TASK № 5
### Write a function keysAndValues(obj) which takes in an object and returns array with object keys and values as separate arrays.  

### Example:
```js
keysAndValues({a: 1, b: 2, c: 3}) // return [['a', 'b', 'c'], [1, 2, 3]] 
```
<hr>

### TASK № 6
### Write function swapMinMax(array) which find the maximum and minimum elements of the array and swap them. 

### Example:
```js
swapMinMax([5,3,8,1,6,3,9,2]);  // return [5,3,8,9,6,3,1,2]  
```
<hr>

### TASK № 7
### Write a function `fizzBuzz` that takes two positive integers as its arguments: `start` and `end`. This function should log to console (use `console.log()`) integer numbers from `start` to `end` (including) except next cases:
1. If number is divisible by 3 - log "Fizz" instead of number; 
2. If number is divisible by 5 - log "Buzz" instead of number; 
3. If number is divisible by 3 and 5 at the same time - log "FizzBuzz" instead of number; 

### Example:
```js
fizzBuzz(2, 4); // logs: 
// 2 
// Fizz 
// 4 

```
<hr>

### TASK № 8
### Create a function add(n) which returns a function that always adds n to any number. 

### Example:
```js
let addOne = add(1); 
addOne(3); // 4 
 
let addThree = add(3); 
addThree(3); // 6 

```
<hr>

### TASK № 9
### Create function "multiplyArrayItems", which multiplies each element in input array by value of 2nd parameter. 
* 1st param - array - list of numeric values 
* 2nd param - number - multiplication factor 
* Returns: updated array 

### Example:
```js
let arr = [1, 10, -50, 0.5]; 
multiplyArrayItems(arr, 2); // [2, 20, -100, 1] 
multiplyArrayItems(arr, -10);// [-10, -100, 500, -5]
```
<hr>

### TASK № 10
### Create a function uniqueValues(arr) which gets an array and returns only unique values. 

### Example:
```js
uniqueValues([3,3,2,2,1]); // [3,2,1] 
uniqueValues([1,2,2,3,3,4,5]); // [1,2,3,4,5]   
```
<hr>
